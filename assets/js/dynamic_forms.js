var $collectionHolder;
// setup an "add" link
var $addDynButton = $('<button type="button" class="add_dyn_link">Añadir</button>');
var $newLinkLi = $('<li></li>').append($addDynButton);

$(document).ready(function () {
    /*
    // Get the ul that holds the collection
    $collectionHolder = $('ul.tags');

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addDynButton.on('click', function(e) {
        // add a new element form (see next code block)
        addDynForm($collectionHolder, $newLinkLi);
    });

    // add a delete link to all of the existing li elements
    $collectionHolder.find('li').each(function() {
        addDynFormDeleteLink($(this));
    });

    // add the "add" anchor and li to the fields ul
    $collectionHolder.append($newLinkLi);

*/
    // ... the rest of the block from above
    var list_name = $('.add-another-collection-widget').attr('data-list');
    var list = $(list_name);
    var removeButtonTags = list.attr('data-remove-button-tags');

    list.children().each(function() {
        addCollectionDeleteLink($(this), removeButtonTags);
    });

    $('.add-another-collection-widget').click(function (e) {
        var list_name = $('.add-another-collection-widget').attr('data-list');
        var list = $(list_name);
        // Try to find the counter of the list
        var counter = list.data('widget-counter') | list.children().length;
        // If the counter does not exist, use the length of the list
        if (!counter) { counter = list.children().length; }

        // grab the prototype template
        var newWidget = list.attr('data-prototype');
        // replace the "__name__" used in the id and name of the prototype
        // with a number that's unique to your emails
        // end name attribute looks like name="contact[emails][2]"
        newWidget = newWidget.replace(/__name__/g, counter);
        // Increase the counter
        counter++;
        // And store it, the length cannot be used if deleting widgets is allowed
        list.data(' widget-counter', counter);

        // create a new list element and add it to the list
        var newElem = jQuery(list.attr('data-widget-tags')).html(newWidget);
        newElem.appendTo(list);

        // add a delete link to the new form
        addCollectionDeleteLink(newElem, removeButtonTags);
    });

    /*
    $('.remove-collection-widget btn').click(function (e) {
        $(this).parents("li").remove();
    });
    */
});

/*
function addDynForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    // You need this only if you didn't set 'label' => false in your field in TaskType
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add" link li
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);

    // add a delete link to the new form
    addDynFormDeleteLink($newFormLi);

}
*/
function addCollectionDeleteLink($dynFormLi, $removeButtonTags) {
    var $removeFormButton = $($removeButtonTags);
    $dynFormLi.append($removeFormButton);

    $removeFormButton.on('click', function(e) {
        // remove the li for the form
        $dynFormLi.remove();
    });
}