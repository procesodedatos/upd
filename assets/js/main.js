// this will let us have a global jQuery object
window.$ = window.jQuery = require("jquery");

// needed for datepicker
window.moment = require('moment');

// bootstrap datepicker component
require("eonasdan-bootstrap-datetimepicker");


$(document).ready(function() {
    // you may need to change this code if you are not using Bootstrap Datepicker
    $('.js-datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
});


