<?php
$servidor_LDAP = "MA1SVAD2.ced.junta-andalucia.es";
$servidor_dominio = "ced.junta-andalucia.es";
//$ldap_dn = "OU=Usuarios,OU=SG-SG,OU=DEPARTAMENTOS,OU=MA1,OU=DPMA,OU=CED-DDTT,DC=ced,DC=junta-andalucia,DC=es";
$ldap_dn = "DC=ced,DC=junta-andalucia,DC=es";
$usuario_LDAP = "aortegag";
$contrasena_LDAP = "Ingenia.00";

echo "<h3>Validar en servidor LDAP desde PHP</h3>";
echo "Conectando con servidor LDAP desde PHP...";

$conectado_LDAP = ldap_connect($servidor_LDAP);
ldap_set_option($conectado_LDAP, LDAP_OPT_PROTOCOL_VERSION, 3);
//ldap_set_option($conectado_LDAP, LDAP_OPT_REFERRALS, 0);

if ($conectado_LDAP)
{
    echo "<br>Conectado correctamente al servidor LDAP " . $servidor_LDAP;

    echo "<br><br>Comprobando usuario y contraseña en Servidor LDAP";
    $autenticado_LDAP = ldap_bind($conectado_LDAP,
        $usuario_LDAP . "@" . $servidor_dominio, $contrasena_LDAP);
    if ($autenticado_LDAP)
    {
        echo "<br>Autenticación en servidor LDAP desde Apache y PHP correcta.";
    }
    else
    {
        echo "<br><br>No se ha podido autenticar con el servidor LDAP: " .
            $servidor_LDAP .
            ", verifique el usuario y la contraseña introducidos";
    }
}
else
{
    echo "<br><br>No se ha podido realizar la conexión con el servidor LDAP: " .
        $servidor_LDAP;
}

