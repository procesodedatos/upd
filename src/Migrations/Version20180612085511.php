<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180612085511 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE publicacion DROP FOREIGN KEY FK_62F2085F233A8A05');
        $this->addSql('DROP INDEX IDX_62F2085F233A8A05 ON publicacion');
        $this->addSql('ALTER TABLE publicacion DROP ambito_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE publicacion ADD ambito_id INT NOT NULL');
        $this->addSql('ALTER TABLE publicacion ADD CONSTRAINT FK_62F2085F233A8A05 FOREIGN KEY (ambito_id) REFERENCES ambito (id)');
        $this->addSql('CREATE INDEX IDX_62F2085F233A8A05 ON publicacion (ambito_id)');
    }
}
