<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180611081926 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tipo_publicacion (id INT AUTO_INCREMENT NOT NULL, descripcion VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ambito (id INT AUTO_INCREMENT NOT NULL, descripcion VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE publicacion (id INT AUTO_INCREMENT NOT NULL, tipo_id INT NOT NULL, ambito_id INT NOT NULL, titulo VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, timestamp DATETIME NOT NULL, INDEX IDX_62F2085FA9276E6C (tipo_id), INDEX IDX_62F2085F233A8A05 (ambito_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE publicacion ADD CONSTRAINT FK_62F2085FA9276E6C FOREIGN KEY (tipo_id) REFERENCES tipo_publicacion (id)');
        $this->addSql('ALTER TABLE publicacion ADD CONSTRAINT FK_62F2085F233A8A05 FOREIGN KEY (ambito_id) REFERENCES ambito (id)');
        $this->insertaTiposPublicacion();
        $this->insertaAmbitos();
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE publicacion DROP FOREIGN KEY FK_62F2085FA9276E6C');
        $this->addSql('ALTER TABLE publicacion DROP FOREIGN KEY FK_62F2085F233A8A05');
        $this->addSql('DROP TABLE tipo_publicacion');
        $this->addSql('DROP TABLE ambito');
        $this->addSql('DROP TABLE publicacion');
    }

    private function insertaTiposPublicacion() {
        $tiposPublicacion = array(
            'Aclaraciones',
            'Acuerdo',
            'Circular',
            'Corrección de errores',
            'Decreto',
            'Decreto-Ley',
            'Directiva',
            'Instrucción',
            'Ley',
            'Orden',
            'Real Decreto',
            'Real Decreto-Ley',
            'Resolución',
            'Tratados',
        );

        foreach ($tiposPublicacion as $tipoPublicacion) {
            $this->addSql("INSERT INTO tipo_publicacion(descripcion) VALUES ('$tipoPublicacion')");
        }
    }

    private function insertaAmbitos() {
        $ambitos = array(
            'Autonómico',
            'Nacional',
            'Provincial',
        );

        foreach ($ambitos as $ambito) {
            $this->addSql("INSERT INTO ambito(descripcion) VALUES ('$ambito')");
        }
    }
}
