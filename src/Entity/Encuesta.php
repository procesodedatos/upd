<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EncuestaRepository")
 */
class Encuesta
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="smallint")
     */
    private $servicio;

    public function getId()
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getServicio(): ?int
    {
        return $this->servicio;
    }

    public function setServicio(int $servicio): self
    {
        $this->servicio = $servicio;

        return $this;
    }

    public static function getArrayServicios() {
        return array(
            'Secretaría General'                            => 1,
            'Administración General y Gestión Económica'    => 2,
            'Gestión de Recursos Humanos'                   => 3,
            'Inspección Educativa'                          => 4,
            'Ordenación Educativa'                          => 5,
            'Planificación y Escolarización'                => 6,
            'Retribuciones'                                 => 7,
        );
    }

    public function getNombreServicio() {
        return array_search($this->servicio, self::getArrayServicios());
    }
}
