<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PublicacionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Publicacion
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titulo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timestamp;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoPublicacion", inversedBy="publicaciones")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tipo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $organo_emisor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $destinatarios;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $detalle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notas;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     * @Assert\Url()
     * @Assert\Expression(
     *     "this.validUrl()",
     *     message="Debe especificar la URL de la publicación"
     * )
     */
    private $url;

    /**
     * @ORM\Column(type="boolean")
     */
    private $publicado;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->timestamp = new \DateTime();
    }

    public function validURL() {
        return !$this->publicado || !empty($this->url);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTimestamp(): ?\DateTimeInterface
    {
        return $this->timestamp;
    }

    public function setTimestamp(\DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function getTipo(): ?TipoPublicacion
    {
        return $this->tipo;
    }

    public function setTipo(?TipoPublicacion $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getOrganoEmisor(): ?string
    {
        return $this->organo_emisor;
    }

    public function setOrganoEmisor(string $organo_emisor): self
    {
        $this->organo_emisor = $organo_emisor;

        return $this;
    }

    public function getDestinatarios(): ?string
    {
        return $this->destinatarios;
    }

    public function setDestinatarios(?string $destinatarios): self
    {
        $this->destinatarios = $destinatarios;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getDetalle(): ?string
    {
        return $this->detalle;
    }

    public function setDetalle(?string $detalle): self
    {
        $this->detalle = $detalle;

        return $this;
    }

    public function getNotas(): ?string
    {
        return $this->notas;
    }

    public function setNotas(string $notas): self
    {
        $this->notas = $notas;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getPublicado(): ?bool
    {
        return $this->publicado;
    }

    public function setPublicado(bool $publicado): self
    {
        $this->publicado = $publicado;

        return $this;
    }
}
