<?php
namespace App\Controller;

use App\Entity\Encuesta;
use App\Repository\EncuestaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Form\EncuestaType;

class EncuestaController extends Controller
{
    /**
     * @Route("/encuesta", name="encuesta_index")
     */
    public function index(Request $request)
    {
        $data = new Encuesta();
        $form = $this->createForm(EncuestaType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();

            return $this->redirectToRoute('encuesta_enviada');
        }

        return $this->render('encuesta/index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/encuesta/enviada", name="encuesta_enviada")
     */
    public function enviada(Request $request)
    {
        return $this->render('encuesta/enviada.html.twig');
    }

    /**
     * @Route("/encuesta/resultados", name="encuesta_resultados")
     */
    public function resultado(EncuestaRepository $encuestas)
    {
        if ($this->isGranted('ROLE_ADMIN') ||
            $this->isGranted('ROLE_ENCUESTAS')) {
            $misEnuestas = $encuestas->findBy(
                [],
                ['servicio' => 'ASC', 'nombre' => 'ASC']
            );

        } else {
            $misEnuestas = null;
        }

        return $this->render('encuesta/list.html.twig', [
            'encuestas' => $misEnuestas,
        ]);
    }

    /**
     * @Route("/encuesta/login", name="encuesta_login")
     */
    public function login (Request $request, AuthenticationUtils $authenticationUtils)
    {

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('encuesta/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }
}
