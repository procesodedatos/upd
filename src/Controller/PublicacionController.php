<?php
namespace App\Controller;

use App\Repository\PublicacionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\PublicacionType;
use App\Entity\Publicacion;
use Psr\Log\LoggerInterface;
class PublicacionController extends Controller
{
    private $logger;
    private $mailer;

    public function __construct(LoggerInterface $logger, \Swift_Mailer $mailer) {
        $this->logger = $logger;
        $this->mailer = $mailer;
    }

    /**
     * @Route("/publicacion", name="publicacion_index")
     */
    public function index(PublicacionRepository $publicaciones)
    {
        if ($this->isGranted('ROLE_ADMIN')) {
            $misPublicaciones = $publicaciones->findBy(
                [],
                ['fecha' => 'DESC', 'email' => 'DESC']
            );
        } else {
            $misPublicaciones = $publicaciones->findBy(
                ['email' => $this->getUser()->getUserName()],
                ['fecha' => 'DESC']
            );
        }
        return $this->render('publicacion/index.html.twig', [
            'publicaciones'     => $misPublicaciones,
            'isAdministrator'  => $this->isGranted('ROLE_ADMIN'),
        ]);
    }

    /**
     * @Route("/publicacion/new" , name="publicacion_new")
     */
    public function new(Request $request)
    {
        $data = new Publicacion();
        $data->setEmail($this->getUser()->getUserName());
        $form = $this->createForm(PublicacionType::class, $data, array(
            'isAdministrator' => $this->isGranted('ROLE_ADMIN'),
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->sendEmail($data);
            $this->addFlash('success', 'Publicación creada correctamente. Se ha enviado un mensaje a Proceso de datos para con su solicitud de publicación. <br> Una vez publicada recibirá un correo de confirmación.');

            return $this->redirectToRoute('publicacion_index');
        }

        return $this->render('publicacion/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Muestra un formulario para editar una publicación
     *
     * @Route("/publicacion/{id}/edit", requirements={"id": "\d+"}, name="publicacion_edit")
     * @Method({"GET", "POST"})
     */
    public function edit(Request $request, Publicacion $publicacion): Response
    {
        // TODO: controlar permisos
        //$this->denyAccessUnlessGranted('edit', $post, 'Posts can only be edited by their authors.');

        $form = $this->createForm(PublicacionType::class, $publicacion, array(
            'isAdministrator' => $this->isGranted('ROLE_ADMIN'),
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //$publicacion->setSlug(Slugger::slugify($post->getTitle()));
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Publicación actualizada correctamente. Se ha enviado un mensaje a Proceso de datos con la actualización de la publicación. <br> Una vez publicada recibirá un correo de confirmación.');

            return $this->redirectToRoute('publicacion_index');
        }

        return $this->render('publicacion/edit.html.twig', array(
            'publicacion'   => $publicacion,
            'form'          => $form->createView(),
        ));
    }

    /**
     * Borra una publicación.
     *
     * @Route("/publicacion/{id}/delete", name="publicacion_delete")
     * @Method("POST")
     * _@_Security("is_granted('delete', publicacion)")
     *
     * The Security annotation value is an expression (if it evaluates to false,
     * the authorization mechanism will prevent the user accessing this resource).
     */
    public function delete(Request $request, Publicacion $publicacion): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('publicacion_index');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($publicacion);
        $em->flush();

        $this->addFlash('success', 'Publicación borrada correctamente. <br>Se ha enviado un correo a Proceso de Datos para cancelar la publicación.');

        return $this->redirectToRoute('publicacion_index');
    }

    private function sendEmail(Publicacion $publicacion)
    {
        $message = (new \Swift_Message('Publicación Solicitada: ' . $publicacion->getTitulo()))
            ->setFrom($this->getParameter('app.email.sender') . '@' . $this->getParameter('app.email.domain'))
            ->setCc($publicacion->getEmail() . '@' . $this->getParameter('app.email.domain'))
            ->setTo($this->getParameter('app.email.receiver') . '@' . $this->getParameter('app.email.domain'))
            ->setBody(
                $this->renderView(
                    'emails/publicacion.html.twig',
                    array('publicacion' => $publicacion)
                ),
                'text/html'
            );

        //$this->mailer->send($message);
        $this->logger->info("No se están enviado mensajes");
    }
}
