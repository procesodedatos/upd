<?php
namespace App\Form;

use App\Entity\Encuesta;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EncuestaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre y apellidos',
            ))
            ->add('servicio', ChoiceType::class, array(
                'label'         => 'Dependencia / Servicio',
                'placeholder'   => 'Selecciona un un valor',
                'choices'       => Encuesta::getArrayServicios(),
            ))
        ;
    }
}