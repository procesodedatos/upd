<?php
namespace App\Form;

use App\Entity\Publicacion;
use App\Entity\TipoPublicacion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PublicacionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isAdministrator = $options['isAdministrator'];
        $builder
            ->add('email', null, array(
                'disabled'      => true,
            ));
        if ($isAdministrator) {
            $builder
                ->add('publicado', CheckboxType::class, array(
                    'required'  => false,
                ));
        } else {
            $builder
                ->add('publicado', HiddenType::class, array(
                    'data'      => 0,
                ));
        }
        $builder
            ->add('url', null, array(
                'disabled'      => !$isAdministrator,
            ))
            ->add('titulo')
            ->add('tipo', EntityType::class, array(
                'class'         => TipoPublicacion::class,
                'choice_label'  => 'descripcion',
                'placeholder'   => 'Selecciona un tipo',
            ))
            ->add('organo_emisor', TextType::class, array(
                'data'          => 'Delegación Territorial de Educación de Málaga'
            ))
            ->add('destinatarios')
            ->add('fecha', DateType::class, array(
                //'widget'        => 'choice',
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker'],
                'format' => 'dd/MM/yyyy',
            ))
            ->add('detalle')
            ->add('notas')
            ->add('emails', CollectionType::class, array(
                'mapped'        => false,
                'prototype'     => true,
                // each entry in the array will be an "email" field
                'entry_type' => EmailType::class,
                // these options are passed to each "email" type
                'entry_options' => array(
                    'attr' => array('class' => 'email-box'),
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class'                => Publicacion::class,
            'translation_domain'        => 'form',
            'isAdministrator'           => false,
        ));
    }
}