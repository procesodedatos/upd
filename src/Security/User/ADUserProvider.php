<?php
namespace App\Security\User;

use App\Security\User\WebserviceUser;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class ADUserProvider implements UserProviderInterface
{
    public function loadUserByUsername($username)
    {
        $admins = array(
            'ANTONIO.ORTEGA.EXT',
        );
        $encuestas = array(
            'ANTONIO.ORTEGA.GONZALEZ.EDU',
            'ANTONIO.ORTEGA.EXT',
            'ARTUROJ.LUIZ',
            'ALVARO.WUCHERPFENNIG.EXT',
        );
        $password = "";
        $salt = null;
        $roles = array('ROLE_USER');

        if (in_array(strtoupper($username), $admins)) {
            $roles[] = 'ROLE_ADMIN';
        }

        if (in_array(strtoupper($username), $encuestas)) {
            $roles[] = 'ROLE_ENCUESTAS';
        }
        return new ADUser($username, $password, $salt, $roles);
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof ADUser) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return ADUser::class === $class;
    }
}
